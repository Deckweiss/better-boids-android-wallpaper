package org.wall.invisible.boids;


import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import java.util.Random;

import static java.lang.Math.sqrt;

class VectorHelper {
    static Random r = new Random();

    public static Vector2D multiplicativeInverse(Vector2D vector) {
        double x;
        double y;

        if (vector.getX() == 0) {
            x = r.nextDouble();
        } else {
            x = 1 / vector.getX();
        }
        if (vector.getY() == 0) {
            y = r.nextDouble();
        } else {
            y = 1 / vector.getY();
        }

        return new Vector2D(x, y);
    }

    // Boid: ist in Area X
    // Checkgt mit 8 Nachbarareas
    // Pro Area:
      // Get Distance from Boids in Area i
      // --> do stuff damit


    public static double toroidalDistance(Vector2D vector1, Vector2D vector2, Vector2D bounds) {
        double dx = Math.abs(vector2.getX() - vector1.getX());
        double dy = Math.abs(vector2.getY() - vector1.getY());

        if (dx > 0.5 * bounds.getX())
            dx = bounds.getX() - dx;

        if (dy > 0.5 * bounds.getY())
            dy = bounds.getY() - dy;

        return sqrt(dx * dx + dy * dy);
    }

    public static Vector2D torodialDirection(Vector2D from, Vector2D to, Vector2D bounds) {
        double x = to.getX() - from.getX();
        double y = to.getY() - from.getY();

        double dx = Math.abs(x);
        double dy = Math.abs(y);

        if (dx > 0.5 * bounds.getX())
            x = bounds.getX() - dx;

        if (dy > 0.5 * bounds.getY())
            y = bounds.getY() - dy;

        return new Vector2D(x, y);


        //if y2 out of bounds
        // --> transpose y2
        // if else:
            // entweder y2 = y2 - höhe | falls TO "weiter unten ist"
            // sonst y2 = y2 + höhe | falls mein TOP "über mit liegt"
        //dasselbe für x2

        //dx = x2 - x1
        //dy = y2 - y1

        //return Vector(dx, dy) <- realtivere Vektor relativ zum FROM
        //return Vector(x+dx, y+dy) <- absoluter Vektor, kann auch out of bounds sein

    }
}
