package org.wall.invisible.boids;

import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.service.wallpaper.WallpaperService;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BoidsWallpaperService extends WallpaperService {
    @Override
    public Engine onCreateEngine() {
        try {
            return new BoidsWallpaperEngine();
        } catch (Exception e) {
            Log.d("Boids", "Could not load wallpaper");
            return null;
        }
    }


    private class BoidsWallpaperEngine extends Engine {
        private double sepF = 1;
        private double aliF = 1;
        private double cohF = 1;
        private int a, r, g, b;
        private boolean mVisible = false;
        private boolean isTouched = false;
        private boolean canvasBoundsSet = false;
        private Vector2D bounds = Vector2D.ZERO;
        private Vector2D touch = Vector2D.NaN;
        private boolean randomColours = false;
        int numberBoids = 30;
        private List<Boid> meineBoids = new ArrayList<>();
        private Paint mPaint = new Paint();
        private final Handler mHandler = new Handler();
        private final Runnable mUpdateDisplay = new Runnable() {

            @Override
            public void run() {
                mainLoop();
            }
        };

        private void mainLoop() {
            //android.os.Debug.waitForDebugger();

            if (canvasBoundsSet) {
                if (meineBoids.isEmpty()) {
                    spawnBoids(numberBoids);
                }
                for (Boid b : meineBoids) {
                    findNeighbour(b);
                }
                for (Boid b : meineBoids) {
                    b.run(isTouched, touch);
                }
            }

            SurfaceHolder holder = getSurfaceHolder();
            Canvas c = null;
            try {
                c = holder.lockCanvas();
                if (c != null) {
                    if (!canvasBoundsSet) {
                        bounds = new Vector2D(c.getWidth(), c.getHeight());
                        canvasBoundsSet = true;
                    }


                    c.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
/*
                    Paint myPaint = new Paint();
                    myPaint.setColor(Color.rgb(100, 200, 150));
                    myPaint.setStyle(Paint.Style.FILL);
                    myPaint.setStrokeWidth(10);
                    c.drawRect(500, 500, 200, 200, myPaint);
*/
                    for (Boid b : meineBoids) {
                        drawBoid(c, b);
                    }
                }
            } finally {
                if (c != null)
                    holder.unlockCanvasAndPost(c);
            }

            mHandler.removeCallbacks(mUpdateDisplay);
            if (mVisible) {
                mHandler.postDelayed(mUpdateDisplay, 25);
            }

        }

        private void spawnBoids(int many) {
            Random r = new Random();
            for (int i = 1; i <= many; i++) {

                meineBoids.add(new NormalBoid(
                        new Vector2D(r.nextInt((int) bounds.getX()), r.nextInt((int) bounds.getY())),
                        new Vector2D(r.nextInt((int) bounds.getX()), r.nextInt((int) bounds.getY())).normalize(),
                        sepF, aliF, cohF,
                        30,
                        3,
                        bounds
                ));
            }
        }

        private void drawBoid(Canvas c, Boid boid) {
            if (!randomColours) {
                mPaint.setARGB(a, r, g, b);
            } else {
                mPaint.setARGB(255, 50, 100, 255);
            }

            float[] pts = boid.getPoints();
            c.drawLines(pts, mPaint);
        }

        private void findNeighbour(Boid boid) {
            boid.getNeighbourBoids().clear();
            for (Boid neighbour : meineBoids) {
                if (VectorHelper.toroidalDistance(boid.getCenter(), neighbour.getCenter(), bounds) < 120
                        && !boid.equals(neighbour)) {
                    boid.addNeighbour(neighbour);
                }
            }
        }

        private void updatePrefs() {
            //Loading the preferences
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(BoidsWallpaperService.this);
            numberBoids = prefs.getInt("number_Boids", 35);
            sepF = prefs.getInt("separation", 25);
            aliF = prefs.getInt("alignment", 100);
            cohF = prefs.getInt("cohesion", 60);
            randomColours = prefs.getBoolean("random_colours", false);
            if (!randomColours) {
                a = prefs.getInt("colour_a", 255);
                r = prefs.getInt("colour_r", 255);
                g = prefs.getInt("colour_g", 255);
                b = prefs.getInt("colour_b", 255);
            }
            meineBoids.clear();
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            mVisible = visible;

            if (visible) {
                updatePrefs();
                mainLoop();

            } else {
                mHandler.removeCallbacks(mUpdateDisplay);
            }

        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            updatePrefs();
            mainLoop();
        }

        @Override
        public void onSurfaceDestroyed(SurfaceHolder holder) {
            super.onSurfaceDestroyed(holder);
            mVisible = false;
            mHandler.removeCallbacks(mUpdateDisplay);
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            mVisible = false;
            mHandler.removeCallbacks(mUpdateDisplay);
        }

        @Override
        public void onTouchEvent(MotionEvent me) {
            try {
                Thread.sleep(25);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            switch (me.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    touch = new Vector2D(me.getX(), me.getY());
                    isTouched = true;
                    break;

                case MotionEvent.ACTION_MOVE:
                    touch = new Vector2D(me.getX(), me.getY());
                    isTouched = true;
                    break;

                case MotionEvent.ACTION_UP:
                    isTouched = false;
                    break;
                default:
                    break;
            }
        }

        public boolean isTouched() {
            return isTouched;
        }

        public Vector2D getTouch() {
            return touch;
        }
    }
}
