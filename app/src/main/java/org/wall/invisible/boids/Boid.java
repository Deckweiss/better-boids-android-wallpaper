package org.wall.invisible.boids;


import android.graphics.Paint;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import java.util.List;

public interface Boid {
    void run(boolean isTouched, Vector2D touch);
    void addNeighbour(Boid neighbour);
    List<Boid> getNeighbourBoids();
    Paint getPaint();
    Vector2D getPos();
    Vector2D getDir();
    float getSize();
    float[] getPoints();
    Vector2D getCenter();
}
