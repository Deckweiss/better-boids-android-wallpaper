package org.wall.invisible.boids;


import android.graphics.Paint;
import android.util.Log;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import java.util.ArrayList;
import java.util.List;

class NormalBoid implements Boid {
    private double armslength;

    private float size;
    private float speed;
    private float speedLimit;
    private float baseSpeed;
    private Vector2D position, direction, center, separation, alignment, cohesion;
    Vector2D bounds;
    private Paint paint;
    private double cohesionFactor, alignmentFactor, separationFactor;
    private List<Boid> neighbourBoids = new ArrayList<>();

    NormalBoid(Vector2D position, Vector2D direction, double separationFactor, double alignmentFactor, double cohesionFactor, float size, float speed, Vector2D bounds) {
        armslength = size * 2;

        this.position = position;
        this.direction = direction;
        this.center = position.add(direction.scalarMultiply(0.5));
        this.cohesionFactor = cohesionFactor / 10000;
        this.alignmentFactor = alignmentFactor / 1000;
        this.separationFactor = separationFactor / 10;
        this.size = size;
        this.speed = speed;
        this.baseSpeed = speed;
        this.speedLimit = speed * 1.5f;
        this.bounds = bounds;
    }

    NormalBoid(Vector2D position, Vector2D direction, double cohesionFactor, double alignmentFactor, double separationFactor, float size, float speed, Vector2D bounds, Paint paint) {
        this(position, direction, cohesionFactor, alignmentFactor, separationFactor, size, speed, bounds);
        this.paint = paint;
    }

    @Override
    public void run(boolean isTouched, Vector2D touch) {
        choseDirection(isTouched, touch);
        step();
    }

    private void choseDirection(boolean isTouched, Vector2D touch) {
        separation = separation().scalarMultiply(separationFactor);
        alignment = alignment().scalarMultiply(alignmentFactor);
        cohesion = cohesion().scalarMultiply(cohesionFactor);

        Vector2D step;

        if (isTouched && VectorHelper.toroidalDistance(touch, center, bounds) <= (7 * size)) {
            Vector2D away = this.center.subtract(touch);
            step = VectorHelper.multiplicativeInverse(away).scalarMultiply(50);
            speed += 0.15f;
        } else {
            step = separation.add(alignment).add(cohesion);
            speed -= 0.1f;
        }

        if (speed > speedLimit) {
            speed = speedLimit;
        } else if (speed <= baseSpeed) {
            speed = baseSpeed;
        }

        direction = direction.add(step).normalize().scalarMultiply(speed);

        /*
        if(direction.equals(Vector2D.ZERO)){
            direction = new Vector2D(1,1);
        }

        if(direction.getNorm() > speedLimit){
            direction = direction.normalize().scalarMultiply(speedLimit);
        }else if(direction.getNorm() < baseSpeed){
            direction = direction.normalize().scalarMultiply(baseSpeed);
        }*/
    }

    private Vector2D separation() {
        Vector2D out = Vector2D.ZERO;

        if (neighbourBoids.isEmpty()) {
            return out;
        }

        for (Boid neighbour : neighbourBoids) {
            double distance = VectorHelper.toroidalDistance(this.center, neighbour.getCenter(), bounds);
            if (distance < this.armslength) {
                //out = out.add(VectorHelper.multiplicativeInverse(this.center.subtract(neighbour.getCenter())));
                out = out.add(VectorHelper.multiplicativeInverse(VectorHelper.torodialDirection(neighbour.getCenter(), this.getCenter(), bounds)));
            }
        }
/*
        if (!out.equals(Vector2D.ZERO)) {
            out = out.normalize();
        }*/

        return out;
    }

    private Vector2D alignment() {
        Vector2D out = Vector2D.ZERO;

        if (neighbourBoids.isEmpty()) {
            return out;
        }

        for (Boid neighbour : neighbourBoids) {
            out = out.add(neighbour.getDir());
        }
        out = out.scalarMultiply(1.0 / neighbourBoids.size());

        return out;
    }

    private Vector2D cohesion() {
        Vector2D out = Vector2D.ZERO;

        if (neighbourBoids.isEmpty()) {
            return out;
        }

        for (Boid neighbour : neighbourBoids) {
            out = out.add(VectorHelper.torodialDirection(this.getCenter(), neighbour.getCenter(), bounds));
        }
        out = out.scalarMultiply(1.0 / neighbourBoids.size());

        return out; //.subtract(this.center);
    }

    private void step() {
        double width = bounds.getX();
        double height = bounds.getY();

        Vector2D step = direction.scalarMultiply(speed);
        position = position.add(step);

        center = position.add(direction.scalarMultiply(0.5));

        double x = center.getX();
        double y = center.getY();

        if (center.getX() < -(size / 2)) {
            x = width;
        } else if (center.getX() > width + (size / 2)) {
            x = 0;
        }

        if (center.getY() < -(size / 2)) {
            y = height;
        } else if (center.getY() > height + (size / 2)) {
            y = 0;
        }

        position = new Vector2D(x, y);
        center = position.add(direction.scalarMultiply(0.5));
    }

    @Override
    public void addNeighbour(Boid neighbour) {
        neighbourBoids.add(neighbour);
    }

    public List<Boid> getNeighbourBoids() {
        return neighbourBoids;
    }

    @Override
    public Paint getPaint() {
        return paint;
    }

    @Override
    public Vector2D getPos() {
        return position;
    }

    @Override
    public Vector2D getDir() {
        return direction;
    }

    @Override
    public float getSize() {
        return size;
    }

    @Override
    public float[] getPoints() {
        float xPos = (float) position.getX();
        float yPos = (float) position.getY();
        Vector2D nose = direction.normalize().scalarMultiply(size);
        float xNose = (float) nose.getX();
        float yNose = (float) nose.getY();
        float xlp = xPos + (yNose / 3) - xNose / 9;
        float ylp = yPos + (-xNose / 3) - yNose / 9;
        float xrp = xPos + (-yNose / 3) - xNose / 9;
        float yrp = yPos + (xNose / 3) - yNose / 9;
        float[] pts = {xPos, yPos, xlp, ylp,
                xPos, yPos, xrp, yrp,
                xrp, yrp, xPos + xNose, yPos + yNose,
                xlp, ylp, xPos + xNose, yPos + yNose
                /*, xPos, yPos,xPos+xDir, yPos+yDir*/};

        return pts;
    }

    public Vector2D getCenter() {
        return center;
    }

    public boolean equals(Object other) {
        if (other == this) return true;
        if (other == null) return false;
        if (getClass() != other.getClass()) return false;
        /*
        NormalBoid otherBoid = (NormalBoid) other;
        return (
                this.armslength == otherBoid.armslength &&
                        this.size == otherBoid.size &&
                        this.speed == otherBoid.speed &&
                        this.baseSpeed == otherBoid.baseSpeed &&
                        this.position == otherBoid.position &&
                        this.direction == otherBoid.direction &&
                        this.cohesionFactor == otherBoid.cohesionFactor &&
                        this.alignmentFactor == otherBoid.alignmentFactor &&
                        this.separationFactor == otherBoid.separationFactor
        );*/
        //Point point = (Point)other;
        //return (x == point.x && y == point.y);
        return false;
    }
}
